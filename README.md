=============================================
INTERNET SECURITY AUDITORS ALERT 2009-004
- Original release date: February 3th, 2010
- Last revised:  February 3th, 2010
- Discovered by: Filippo Baruffaldi
- Severity: 6.3/10 (CVSS scored)
=============================================

I. VULNERABILITY
-------------------------
Italian ISP - Fastweb S.p.A. - Fiber Channel Network Vulnerability

II. BACKGROUND
-------------------------
Fastweb S.p.A. is an Italian Internet Services Provider who built
a fiber infrastructure over the Italian territory.

III. DESCRIPTION
-------------------------
Si puo' ottenere la connessione gratuita con qualche piccolo
accorgimento a causa della "generosa" netmask. Associano l'IP all'utenza.

IV. PROOF OF CONCEPT
-------------------------
Cercare un IP valido e usarlo.

Tested with Pirelli Router and Catalyst 48

V. BUSINESS IMPACT
-------------------------
The attacker may stole your connection and some personal information
also. ( problema privacy )

VI. SYSTEMS AFFECTED
-------------------------
Seems to be affected just the fiber network and not the ADSL.

VII. SOLUTION
-------------------------
Fastweb may block non-DHCP requests, or simply set the right netmask

VIII. REFERENCES
-------------------------
http://www.fastweb.it

IX. CREDITS
-------------------------
This vulnerability has been discovered by Filippo Baruffaldi (filippo (at) baruffaldi (dot) info).

X. REVISION HISTORY
-------------------------
Dicember  2, 2010: Initial release

XI. DISCLOSURE TIMELINE
-------------------------
November  2, 2010: ISP contacted

XII. LEGAL NOTICES
-------------------------
The information contained within this advisory is supplied "as-is"
with no warranties or guarantees of fitness of use or otherwise.
Internet Security Auditors accepts no responsibility for any damage
caused by the use or misuse of this information.